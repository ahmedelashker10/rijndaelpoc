//
//  ViewController.swift
//  RijndaelPOC
//
//  Created by Ahmed Elashker on 1/31/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var lblEncryption: UILabel!
    @IBOutlet weak var lblDecryption: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func encrypt(_ sender: Any) {
        let AES = CryptoJS.AES()
        lblEncryption.text = AES.encrypt(txtField.text!, secretKey: "TestEnc123")
    }
    
    @IBAction func decrypt(_ sender: Any) {
        let AES = CryptoJS.AES()
        lblDecryption.text = AES.decrypt(lblEncryption.text!, secretKey: "TestEnc123")
    }
}

